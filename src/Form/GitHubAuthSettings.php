<?php

namespace Drupal\markdown_exporter\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Component\Utility\Crypt;
use Drupal\markdown_exporter\MarkdownGenerationHelperInterface;

/**
 * Class GitHubAuthSettings.
 *
 * Configure GitHub Auth settings for this site.
 *
 * @internal
 */
class GitHubAuthSettings extends ConfigFormBase {

  /**
   * The drupal messenger.
   *
   * @var Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The markdoen generator helper.
   *
   * @var Drupal\markdown_exporter\MarkdownGenerationHelper
   */
  protected $mdGeneratorHelper;

  /**
   * Class constructor.
   */
  public function __construct(MessengerInterface $messanger_manager, MarkdownGenerationHelperInterface $md_generator) {
    $this->messenger = $messanger_manager;
    $this->mdGeneratorHelper = $md_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $messenger = $container->get('messenger');
    $mdHelper = $container->get('markdown_exporter.marekdown_helper');

    return new static($messenger, $mdHelper);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'markdown_exporter_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['markdown_exporter.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Retrieve non editable config.
    $markdown_exporter_config = $this->config('markdown_exporter.settings');

    // If  github application oauth token
    // is not found then display a message.
    if ($markdown_exporter_config->get('markdown_file_commit', FALSE) && !$markdown_exporter_config->get('github_oauth_app_clientid', FALSE)) {
      $this->messenger->addMessage(
        $this->t('GitHub OAuth application token missing.
         Please get a token from <a href="
         https://github.com/settings/applications/new">
         https://github.com/settings/applications/new</a>.'),
          'status'
      );
    }

    // Adding form elements.
    $form['info'] = [
      '#markup' => $this->t('<h1 style="color: #212121;">GitHub Application OAuth Token</h1><p style="color: #3E2723;">Please ensure you use the following configurations when registering the OAuth application:<br><ul><li>Homepage URL: <strong>:url</strong></li><li>Authorization callback URL: <strong>:url/admin/config/development/markdown_export/github-authentication/reauthorize</strong></li></ul></p>',
      [':url' => $GLOBALS['base_url']]
      ),
    ];
    $form['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GitHub OAuth Token Client ID'),
      '#required' => TRUE,
      '#default_value' => $markdown_exporter_config->get('github_oauth_app_clientid', FALSE),
      '#description' => $this->t('GitHub Client ID'),
    ];
    $form['secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GitHub OAuth Token Client Secret'),
      '#required' => TRUE,
      '#default_value' => $markdown_exporter_config->get('github_oauth_app_clientsecret', FALSE),
      '#description' => $this->t('GitHub Client Secret'),
      '#suffix' => '<br>',
    ];

    // Once the tokens are provided,
    // display the next steps.
    if ($markdown_exporter_config->get('github_oauth_app_clientid') && $markdown_exporter_config->get('github_oauth_app_clientsecret')) {
      // Generate a random state
      // string for github.
      $markdown_exporter_config
        ->set('github_state_var', Crypt::hashBase64(time()))
        ->save();
      // Url to authenticate with github.
      $url = 'https://github.com/login/oauth/authorize?client_id=' .
       $markdown_exporter_config->get('github_oauth_app_clientid') .
        '&scope=repo&state=' .
         $markdown_exporter_config->get('github_state_var');

      // Github authorization link.
      $form['githubAuthorize'] = [
        '#markup' => '<a href="' . Url::fromUri($url)->toString() . '">' . $this->t('Authorize GitHub') . '</a>',
        '#suffix' => '<br><br><br>',
      ];

      // If github auth token, is present, display the next steps.
      if ($markdown_exporter_config->get('github_access_token')) {
        // Saving GitHub repo in config for the first time
        // to retrieve it from next time onwardsfetch repository list.
        $repoList = $this->mdGeneratorHelper->getJsonResponse('user/repos');
        $this->messenger->addMessage(
          $this->t('Repository list fetched from GitHub.'),
          'status'
        );
        // Store list in a variable to reduce future api calls.
        $repoArr = [];
        foreach ($repoList as $val) {
          $repoArr[$val['full_name']] = $val['name'];
        }
        $markdown_exporter_config
          ->set('github_repo_list', $repoArr)
          ->save();
        // Building the remaining form.
        $form['next_info_2'] = [
          '#markup' => $this->t('<h2 style="color: #212121;">Select Repository</h2><p style="color: #3E2723;">Select the repository you wish to push the generated markdown files to.</p>'),
        ];
        $pushToRepo = $markdown_exporter_config->get('github_pushto_repo');
        $form['select_repository'] = [
          '#type' => 'select',
          '#title' => $this->t('Select Repository'),
          '#default_value' => $pushToRepo ? $pushToRepo : FALSE,
          '#options' => array_merge([$this->t('-- Select --')], $markdown_exporter_config->get('github_repo_list')),
          '#description' => $this->t('Select Repository get field list.'),
          '#ajax' => [
            // Could also use [get_class($this), 'fetchRepoDirs'].
            'callback' => '::fetchRepoDirs',
            'wrapper' => 'repo-field-wrapper',
          ],
        ];

        // Add a wrapper that can be replaced with new HTML
        // by the ajax callback. This is given the ID that
        // was passed to the ajax callback in the '#ajax'
        // element above.
        $form['repo_field_wrapper'] = [
          '#type' => 'container',
          '#attributes' => ['id' => 'repo-field-wrapper'],
        ];
        // Add a field element.
        $selectRepository = $form_state->getValue('select_repository');
        $selectedRepository = $markdown_exporter_config->get('github_pushto_repo');
        $savedDirecory = $markdown_exporter_config->get('github_pushto_repo_dir');
        $savedBranch = $markdown_exporter_config->get('github_pushto_repo_branch');

        if (count($selectRepository) > 0 || count($selectedRepository) > 0) {
          // If (!empty($selectRepository)) {.
          $selectRepository = (count($selectRepository) == 0) ?
            $selectedRepository : $selectRepository;
          $dirList = $this->getRepositoryDirBranches($selectRepository, 'DIR');
          $branchList = $this->getRepositoryDirBranches($selectRepository, 'BRC');
          $form['repo_field_wrapper']['select_repository_dir'] = [
            '#type' => 'select',
            '#multiple' => TRUE,
            '#title' => $this->t('Select Repository Directory'),
            '#description' => $this->t('Select Repository Directory'),
            '#options' => $dirList,
            '#default_value' => $savedDirecory,
          ];
          $form['repo_field_wrapper']['select_repository_branches'] = [
            '#type' => 'select',
            '#title' => $this->t('Select Repository Branch'),
            '#description' => $this->t('Select Repository Branch'),
            '#options' => $branchList,
            '#default_value' => $savedBranch,
          ];
          if ($selectRepository == $pushToRepo) {
            $form['repo_field_wrapper']['select_repository_dir']['#default_value'] = $savedDirecory;
            $form['repo_field_wrapper']['select_repository_branches']['#default_value'] = $savedBranch;
          }
        }

      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('markdown_exporter.settings')
      ->set('github_oauth_app_clientid', $form_state->getValue('token'))
      ->set('github_oauth_app_clientsecret', $form_state->getValue('secret'))
      ->set('markdown_file_overwrite', $form_state->getValue('markdown_file_overwrite'))
      ->set('markdown_file_commit', $form_state->getValue('markdown_file_commit'))
      ->set('github_pushto_repo', $form_state->getValue('select_repository'))
      ->set('github_pushto_repo_dir', $form_state->getValue('select_repository_dir'))
      // ->set('github_pushto_repo_dir', 'public')
      ->set('github_pushto_repo_branch', $form_state->getValue('select_repository_branches'))
      // ->set('github_pushto_repo_branch', 'master')
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Ajax callback for the repository dropdown.
   */
  public function fetchRepoDirs(array $form, FormStateInterface $form_state) {
    return $form['repo_field_wrapper'];
  }

  /**
   * Return directory and branches for selected repository.
   *
   * @param string $selectRepository
   *   Selected repository.
   * @param string $type
   *   Type to fetch (DIR: direcory, BRC: branch)
   *
   * @return array
   *   list of branches or directories
   */
  protected function getRepositoryDirBranches($selectRepository, $type = "") {
    /* fetch the object tree list from github */
    if ($type == "") {
      return [];
    }

    $optionsListArr = [];

    // Call getJsonResponse helper service to retrieve details
    // DIR: represents directory to fetch for repository
    // BRC: stands for branch
    // getJsonResponse() returns only branches whose protected != 1.
    switch ($type) {
      case 'DIR':
        $url_frag = 'repos/' . $selectRepository . '/contents/';
        $resp = $this->mdGeneratorHelper->getJsonResponse($url_frag);
        /* format into dir list only */
        $optionsListArr['<root>'] = '<root>';
        foreach ($resp as $val) {
          if ($val['type'] == 'dir') {
            $optionsListArr[$val['path']] = $val['name'];
            $url_frag_ch = 'repos/' . $selectRepository . '/git/trees/' . $val['sha'] . '?recursive=1';
            $childs = $resp = $this->mdGeneratorHelper->getJsonResponse($url_frag_ch);
            foreach ($childs['tree'] as $v) {
              if ($v['type'] == 'tree') {
                $optionsListArr[$val['path'] . "/" . $v['path']] = " - " . $v['path'];
              }
            }
          }
        }
        break;

      case 'BRC':
        $url_brac = 'repos/' . $selectRepository . '/branches';
        $branches = $this->mdGeneratorHelper->getJsonResponse($url_brac);
        foreach ($branches as $v) {
          if ($v['protected'] != 1) {
            $optionsListArr[$v['name']] = $v['name'];
          }
        }
        break;
    }

    return $optionsListArr;
  }

}
