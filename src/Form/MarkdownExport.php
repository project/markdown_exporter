<?php

namespace Drupal\markdown_exporter\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\markdown_exporter\MarkdownExporterGeneratorInterface;

/**
 * Class MarkdownExport.
 */
class MarkdownExport extends FormBase {

  /**
   * The markdoen generator helper.
   *
   * @var Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The markdoen generator helper.
   *
   * @var Drupal\markdown_exporter\MarkdownGenerationHelper
   */
  protected $mdExportGenerator;

  /**
   * Class constructor.
   */
  public function __construct(
    EntityTypeManager $entity_type_manager,
    MarkdownExporterGeneratorInterface $md_generator
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->mdExportGenerator = $md_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entityTypeManager = $container->get('entity_type.manager');
    $mdHelper = $container->get('markdown_exporter.marekdown_generator');

    return new static($entityTypeManager, $mdHelper);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'markdown_exporter_export';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $node_types = $this->entityTypeManager->getStorage('node_type')
      ->loadMultiple();
    // If you need to display them in a drop down:
    $content_types = [];
    foreach ($node_types as $node_type) {
      $content_types[$node_type->id()] = $node_type->label();
    }
    $form['types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content Types'),
      '#default_value' => [],
      '#options' => $content_types,
      '#description' => $this->t('Choose content types to be exported.'),
    ];
    // Submit button.
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Run Export'),
      '#suffix' => $this->t('<hr style="color:#CCC"><p><em style="font-size:0.75rem">**Use custom drush command <strong>"drush mde"</strong> or <strong>"drush markdown-export"</strong> if nodes to be exported are significantly large in number. To export specific content type supply the content type name with the drush command (e.g. drush mde page). For multiple content types, values needs to seperated by single blank space (e.g. drush mde page article)</em></p>'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $get_types = $form_state->getValue('types');
    $this->mdExportGenerator->bulkExport($get_types);
  }

}
