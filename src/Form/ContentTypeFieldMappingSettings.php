<?php

namespace Drupal\markdown_exporter\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Component\Serialization\Json;

/**
 * Class ContentTypeFieldMappingSettings.
 *
 * @package Drupal\markdown_exporter\Controller
 */
class ContentTypeFieldMappingSettings extends ConfigFormBase {

  /**
   * The markdoen generator helper.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The markdoen generator helper.
   *
   * @var Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The drupal messenger.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'markdown_exporter_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['markdown_exporter.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Retrieve non editable configuration
    // if saved and parse the configuration
    // to convert into an array.
    $markdown_exporter_config = $this->config('markdown_exporter.settings');
    $savedFieldConfig = $markdown_exporter_config->get('markdown_exporter_fields');
    $savedFieldConfigArr = Json::decode($savedFieldConfig, TRUE);

    $content_types = $this->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();
    $types = [];
    foreach ($content_types as $val) {
      $types[$val->id()] = $val->label();
    }
    $entityKey = $this->getRequest()->query->get('entity_type');
    $entity_type_selected = (isset($entityKey) && !empty($entityKey)) ? $entityKey : '';

    /* creating form */
    $form['info'] = [
      '#markup' => $this->t('<h1 style="color: #212121;">Content Type Field Mapping</h1><p style="color: #3E2723;">You can choose a Content Type and map the fields to the markdown file. The choices you make in this section will generate a JSON configuration that you can manually modify from the setting tab.</p><p>Detailed instruction will be updated soon.</p>'),
    ];
    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Content Types'),
      '#default_value' => $entity_type_selected,
      '#options' => array_merge([$this->t('-- Select --')], $types),
      '#description' => $this->t('Select content types get field list.'),
      '#ajax' => [
        // Could also use [get_class($this), 'listContentTypeFields'].
        'callback' => '::listContentTypeFields',
        'wrapper' => 'content-type-field-wrapper',
      ],
    ];
    // Add a wrapper that can be replaced with new HTML by the ajax callback.
    // This is given the ID that was passed to the ajax callback in the '#ajax'
    // element above.
    $form['content_type_field_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'content-type-field-wrapper'],
    ];

    // Add a field element.
    $entityType = $form_state->getValue('entity_type');
    if (!empty($entityType)) {
      $formatters = $savedFieldConfigArr[$entityType]['formatter'];
      $content = $savedFieldConfigArr[$entityType]['content'];
      // Echo '<pre>'; print_r($formatters);
      // echo '<pre>'; print_r($this->getContentTypeFields($entityType)); exit;
      // Formatter fields.
      $form['content_type_field_wrapper']['formatters'] = [
        '#type' => 'details',
        '#title' => $this->t('Formatter Fields'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      ];
      $form['content_type_field_wrapper']['formatters']['formatter_fields'] = [
        '#type' => 'checkboxes',
        '#multiple' => TRUE,
        '#options' => $this->getContentTypeFields($entityType),
        '#default_value' => $formatters,
      ];

      // Content fields.
      $form['content_type_field_wrapper']['bodyfields'] = [
        '#type' => 'details',
        '#title' => $this->t('Body Fields'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      ];
      $form['content_type_field_wrapper']['bodyfields']['content_fields'] = [
        '#type' => 'checkboxes',
        '#multiple' => TRUE,
        '#options' => $this->getContentTypeFields($entityType),
        '#default_value' => $content,
      ];
    }

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $markdown_exporter_config = $this->config('markdown_exporter.settings')->get('markdown_exporter_fields');
    $entityFields = (!empty($markdown_exporter_config)) ? (array) Json::decode($markdown_exporter_config) : [];
    $entityType = $form_state->getValue('entity_type');

    $formatterFields = array_values(array_filter($form_state->getValue('formatter_fields')));
    $contentFields = array_values(array_filter($form_state->getValue('content_fields')));

    if (!empty($formatterFields) || !empty($contentFields)) {
      $entityFields[$entityType]->formatter = $formatterFields;
      $entityFields[$entityType]->content = $contentFields;
    }
    else {
      unset($entityFields[$entityType]);
    }

    $this->config('markdown_exporter.settings')
      ->set('markdown_exporter_fields', \json_encode($entityFields))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Ajax callback for the color dropdown.
   */
  public function listContentTypeFields(array $form, FormStateInterface $form_state) {
    return $form['content_type_field_wrapper'];
  }

  /**
   * Return fields under a content type.
   *
   * @param string $entityType
   *   Entity type.
   *
   * @return array
   *   Fields array
   */
  protected function getContentTypeFields($entityType) {
    $bundleFields = [];
    $defaultFields = [
      'type' => $this->t('Content type'),
      'title' => $this->t('Title'),
      'created' => $this->t('Created date'),
    ];
    $entity_type_id = 'node';
    $fieldsDef = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $entityType);
    foreach ($fieldsDef as $field_name => $field_definition) {
      if (!empty($field_definition->getTargetBundle())) {
        $bundleFields[$field_name] = $field_definition->getLabel();
      }
    }
    $fieldArr = \array_merge($defaultFields, $bundleFields);

    return $fieldArr;
  }

}
