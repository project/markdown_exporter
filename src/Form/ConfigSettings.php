<?php

namespace Drupal\markdown_exporter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigSettings.
 *
 * Configure JSON:API settings for this site.
 *
 * @internal
 */
class ConfigSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'markdown_exporter_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['markdown_exporter.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $markdown_exporter_config = $this->config('markdown_exporter.settings');

    $form['markdown_exporter_fields'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Content Types & Fields JSON'),
      '#description' => $this->t('JSON string representing the content types & fields to be included in the export. For each content type, JSON string will have two parts: <b>formatter</b>  & <b>content</b> to hold the fields forming Markdown Header & Body part respectively. Sample JSON: \'{"page":{"formatter":["type", "title","created"],"content":["body"]}}\''),
      '#default_value' => $markdown_exporter_config->get('markdown_exporter_fields') === NULL ? '{"page":{"formatter":["type", "title","created"],"content":["body"]}}' : $markdown_exporter_config->get('markdown_exporter_fields'),
    ];
    $form['markdown_exporter_dir'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Markdown files directory'),
      '#description' => $this->t('Name of the directory under files folder where exported markdown files will be stored.'),
      '#default_value' => $markdown_exporter_config->get('markdown_exporter_dir') === NULL ? 'markdown_export' : $markdown_exporter_config->get('markdown_exporter_dir'),
    ];
    $form['markdown_file_overwrite'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Overwrite older version of markdown files'),
      '#default_value' => $markdown_exporter_config->get('markdown_file_overwrite') === NULL ? TRUE : $markdown_exporter_config->get('markdown_file_overwrite'),
      '#description'   => $this->t("Older versions of markdown files will get overwritten by the newer versions generated with each run."),
    ];
    $form['markdown_file_commit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable auto push Markdown files to GitHub repository'),
      '#default_value' => $markdown_exporter_config->get('markdown_file_commit', FALSE),
      '#description'   => $this->t("On a content change, fresh markdown files will be generated for the node and pushed to GitHub repository."),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('markdown_exporter.settings')
      ->set('markdown_exporter_fields', $form_state->getValue('markdown_exporter_fields'))
      ->set('markdown_exporter_dir', $form_state->getValue('markdown_exporter_dir'))
      ->set('markdown_file_overwrite', $form_state->getValue('markdown_file_overwrite'))
      ->set('markdown_file_commit', $form_state->getValue('markdown_file_commit'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
