<?php

/**
 * @file
 * Exporting Drupal contents in markdown format.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;

/**
 * Implements hook_help().
 */
function markdown_exporter_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'markdown_exporter.config':
      $output = '';
      $output .= '<h3>' . t('Synopsis') . '</h3>';
      $output .= '<p>' . t("The purpose of this module is to export Drupal 8 nodes in the form of Markdown files. The principal objective behind this module to facilitate the JAMStack based architecture where decoupling of Drupal (from it's presentation layer) is required.</p>");
      $output .= '<p>' . t('This module can generate markdown files in both online and offline(via drush command) mode and these files will be stored under files directory.</p>');
      $output .= '<h3>' . t('Requirements') . '</h3>';
      $output .= '<p>' . t('<a href="https://packagist.org/packages/pixel418/markdownify" target="_blank">Markdownify</a> is a packagist repo, which helps to convert html to markdown') . '</p>';
      $output .= '<h3>' . t('Configurations') . '</h3>';
      $output .= '<p>' . t('JSON string representing the content types & fields to be included in the export. For each content type, JSON string will have two parts: formatter & content to hold the fields forming Markdown Header & Body part respectively. Sample JSON: {"page":{"formatter":["type", "title","created"],"content":["body"]}}') . '</p>';

      return $output;

    case 'markdown_exporter.github_auth':
      $output = '';
      $output .= '<h3>' . t('Requirements') . '</h3>';
      $output .= '<p>' . t('After markdowns generated, wheather those need to push to Git for building static site.</p>');
      $output .= '<p>' . t('These configurations helps to connect to GIT repo, branch and directory.</p>');

      return $output;

    case 'markdown_exporter.content_type':
      $output = '';
      $output .= '<h3>' . t('Requirements') . '</h3>';
      $output .= '<p>' . t('Markdown contains two parts, "formatter" and "content".</p>');
      $output .= '<p>' . t('These configurations helps to identify which field to assign which section according to need.</p>');

      return $output;

    case 'markdown_exporter.export':
      $output = '';
      $output .= '<p>' . t('Bulk export will generate markdown of nodes as per selected content types below.</p>');

      return $output;

    case 'markdown_exporter.download':
      $output = '';
      $output .= '<p>' . t('Provide option to download markdown files.</p>');

      return $output;

    case 'markdown_exporter.github_auth_redirect':
      $output = '';
      $output .= '<p>' . t('Redirect after Git authorization.</p>');

      return $output;
  }
}

/**
 * Implements hook_node_update().
 */
function markdown_exporter_node_update(NodeInterface $node) {
  \Drupal::service('markdown_exporter.marekdown_generator')->generateMarkdownNodeContent($node);
}

/**
 * Implements hook_node_insert().
 */
function markdown_exporter_node_insert(NodeInterface $node) {
  \Drupal::service('markdown_exporter.marekdown_generator')->generateMarkdownNodeContent($node);
}
