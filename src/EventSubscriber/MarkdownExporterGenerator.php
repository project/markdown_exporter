<?php

namespace Drupal\markdown_exporter\EventSubscriber;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\markdown_exporter\MarkdownGenerationHelperInterface;
use Drupal\markdown_exporter\MarkdownExporterGeneratorInterface;

/**
 * Class MarkdownExporterGenerator.
 *
 * @package Drupal\markdown_exporter\EventSubscriber
 */
class MarkdownExporterGenerator implements ContainerAwareInterface, MarkdownExporterGeneratorInterface {

  use ContainerAwareTrait;

  /**
   * User account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The markdown configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The drupal messenger.
   *
   * @var Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The logger factory.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The file system.
   *
   * @var Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The markdoen generator helper.
   *
   * @var Drupal\markdown_exporter\MarkdownGenerationHelper
   */
  protected $mdGeneratorHelper;

  /**
   * The markdoen generator helper.
   *
   * @var Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The path alias manager.
   *
   * @var Drupal\Core\Path\AliasManager
   */
  protected $pathAliasManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
        ConfigFactoryInterface $config_factory,
        AccountInterface $account,
        MessengerInterface $messanger_manager,
        LoggerChannelFactory $factory,
        FileSystemInterface $file_manager,
        MarkdownGenerationHelperInterface $md_generator,
        EntityTypeManagerInterface $enity_type_manager,
        AliasManagerInterface $path_manager
  ) {
    $this->config = $config_factory->get('markdown_exporter.settings');
    $this->account = $account;
    $this->messenger = $messanger_manager;
    $this->loggerFactory = $factory;
    $this->fileSystem = $file_manager;
    $this->mdGeneratorHelper = $md_generator;
    $this->entityTypeManager = $enity_type_manager;
    $this->pathAliasManager = $path_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('current_user')
    );
  }

  /**
   * Creating content files in MD format.
   *
   * Converts the content of the specified types to markdown.
   *
   * Creates a zip containing them.
   *
   * @param array $getTypes
   *   Content types.
   * @param string $mode
   *   Mode=cli if running through drush.
   */
  public function bulkExport(array $getTypes = [], $mode = '') {
    $mdDir = 'public://' . $this->config->get('markdown_exporter_dir');
    $field_config = $this->config->get('markdown_exporter_fields');
    $success = TRUE;

    $this->fileSystem->prepareDirectory($mdDir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);

    $types = array_values($getTypes);
    array_pop($types);

    $node_details = $this->entityTypeManager->getStorage('node')
      ->loadByProperties(['type' => $types, 'status' => 1]);

    $entity_fields = \json_decode($field_config, TRUE);

    if (!empty($node_details)) {
      foreach ($node_details as $node) {
        $urlAlias = $this->pathAliasManager->getAliasByPath('/node/' . $node->id());

        // Formatting the alias.
        $urlAlias_formatted = strpos($urlAlias, '/') ? str_replace('/', "-", $urlAlias) : $urlAlias;

        // Content file in .md format.
        $file_name = $urlAlias_formatted . '.md';

        $contents = $this->mdGeneratorHelper->mdContentGenerate($urlAlias, $node, $entity_fields);

        // And create the file.
        $replace = ($this->config->get('markdown_file_overwrite') == TRUE) ?
        FileSystemInterface::EXISTS_REPLACE : FileSystemInterface::EXISTS_RENAME;
        if ($success) {
          \Drupal\Core\File\FileSystem::chmod($mdDir);
          $base_file = $mdDir . $file_name;

          $this->fileSystem->saveData(
          $contents, $base_file, $replace
          );
          $file = File::create([
            'uri' => $base_file,
            'uid' => $this->account->id(),
            'status' => FILE_STATUS_PERMANENT,
          ]);

          $this->mdGeneratorHelper->pushToGitRepo($base_file, $file);
        }
      }
      $msg = ($mode == 'cli') ? 'MarkDown files are generated & saved in files/' . $this->config->get('markdown_exporter_dir', 'markdown_export')
      : $msg = 'MarkDown files are generated! <a href="' . Url::fromRoute('markdown_exporter.download')->toString() . '">Click </a> to download the files in ziped format';

      $this->messenger->addMessage($msg, 'status');
      /* Drupal watchdog log */
      $this->loggerFactory->get('markdown_exporter')->notice('MarkDown files are generated');

      $this->messenger->addMessage("Files committed to repository", 'status');
    }

    return FALSE;
  }

  /**
   * Creating single node content files in MD format.
   *
   * Converts the content of the node to markdown.
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   Node entity.
   * @param string $mode
   *   Mode=cli for drush.
   */
  public function generateMarkdownNodeContent(EntityInterface $entity, $mode = NULL) {
    $markdownFields = \json_decode($this->config->get('markdown_exporter_fields'), TRUE);

    if (!array_key_exists($entity->bundle(), $markdownFields) && empty($markdownFields[$entity->bundle()])) {
      return;
    }

    $mdDir = 'public://' . $this->config->get('markdown_exporter_dir');
    $success = TRUE;

    $this->fileSystem->prepareDirectory($mdDir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);

    $urlAlias = $this->pathAliasManager->getAliasByPath('/node/' . $entity->id());

    // Formatting the alias.
    $urlAliasFormatted = strpos($urlAlias, '/') ? str_replace('/', "-", $urlAlias) : $urlAlias;

    // Content file in .md format.
    $file_name = $urlAliasFormatted . '.md';

    $contents = $this->mdGeneratorHelper->mdContentGenerate($urlAlias, $entity, $markdownFields);

    $replace = ($this->config->get('markdown_file_overwrite') == TRUE) ?
                FileSystemInterface::EXISTS_REPLACE : FileSystemInterface::EXISTS_RENAME;
    if ($success) {
      \Drupal\Core\File\FileSystem::chmod($mdDir);
      $base_file = $mdDir . $file_name;
      $this->fileSystem->saveData(
        $contents, $base_file, $replace
      );
      $file = File::create([
        'uri' => $base_file,
        'uid' => $this->account->id(),
        'status' => FILE_STATUS_PERMANENT,
      ]);
      if (NULL !== $file->getFileUri()) {
        $msg = ($mode == 'cli') ? 'MarkDown files are generated & saved in'
          . ' files/' . variable_get('markdown_exporter_dir', 'markdown_export')
          : $msg = 'MarkDown file has been generated!';

        $this->messenger->addMessage($msg, 'status');
        $this->mdGeneratorHelper->pushToGitRepo($file_name, $file, $entity->id());
        $this->messenger->addMessage("Files committed to repository", 'status');
      }
      else {
        $this->messenger->addMessage('Error in generating markdown', 'status');
      }
    }
  }

}
