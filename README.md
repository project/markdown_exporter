---
uri: '/README.md'
type: 'file'
title: 'README file for Markdown Exporter'
created: '1568715498'
author: ramit.mitra
---

Synopsis
--------
The purpose of this module is to export Drupal 8 nodes in the form of Markdown 
files. The principal objective behind this module to facilitate the JAMStack 
based architecture where decoupling of Drupal (from it's presentation layer) 
is required.

This module can generate markdown files in both online and offline(via drush 
command) mode and these files will be stored under files directory.

Requirements
------------
Markdownify is a packagist repo, which helps to convert html to markdown

Recommended modules
-------------------
NA

Roadmap
-------
 * D8 module should have the Markdownify as composer based package
 * BitBucket API to integrate
 * All field types to match, reference fields are yet to be done

Known problems
--------------
Reference fields are not converting

Credits
-------
 * Ramit Mitra<ramit.mitra> actually published the project with all todos 
 completed
 * Samajit Das<samajit> started the project with an idea behind JAMstack

Resources
---------
 * The packagist repo: https://packagist.org/packages/pixel418/markdownify
 * Read this article behind the idea of JAMstack: 
 https://medium.com/@ruddhani/idea-behind-jamstack-markdown-and-a-drupal-module-f6f238bf609b

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Package depedency
 * Configuration
 * Drush commands
 * Maintainers

INTRODUCTION
------------

Markdown Exporter : A module empowering drupal user to export contents in the 
form of Markdown files.

The purpose of this module is to export Drupal 8 nodes in the form of Markdown 
files. The principal objective behind this module to facilitate the JAMStack 
based architecture where decoupling of Drupal (from it's presentation layer) 
is required.

This module can generate markdown files in both online and offline(via drush 
command) mode and these files will be stored under files directory.

* For a full description of the module, visit the project page:
   https://www.drupal.org/project/markdown_exporter

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/markdown_exporter

REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/docs/extending-drupal/installing-modules for 
   further information.

PACKAGE DEPEDENCY
-----------------
 * https://packagist.org/packages/pixel418/markdownify

CONFIGURATION
-------------

  * Go to "admin/config/system/markdown-export/settings" change desired values
  * For "Content Types & Fields JSON" go to "admin/config/system/markdown-
  export/content-type-settings"
  * Content Types & Fields JSON: Content types & fields to be exported can be 
  provided in form of JSON string as like {"page":{"formatter":["type", 
  "title","created"],"content":["body"]}}. This JSON format has    
    two parts: formatter & content to specify the fields forming Markdown 
    Header & Body part respectively.
  * Markdown files directory: name of the dirctory(under files folder) to 
  store generated markdown files. The module can create the directory if 
  not exists
 

DRUSH COMMANDS
--------------

- drush mde -> this will export all the nodes in markdown format and save .md 
files in the specified directory under /files folder
- drush mde page --> this will export all the nodes of "page" content type
- drush mde page article --> this will export all the nodes of "page" & 
"article" content types

MAINTAINERS
-----------

- Samajit Das (samajit) - https://www.drupal.org/u/samajit
- Ramit Mitra (ramitmitra) - https://www.drupal.org/u/ramitmitra
- Aniruddha Banerjee (aniruddhab) - https://www.drupal.org/u/aniruddhab

  
  

  
  

