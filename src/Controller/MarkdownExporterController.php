<?php

namespace Drupal\markdown_exporter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MarkdownExporterController.
 *
 * @package Drupal\markdown_exporter\Controller
 */
class MarkdownExporterController extends ControllerBase {

  /**
   * The markdown configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The markdown configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $configEditable;

  /**
   * The drupal messenger.
   *
   * @var Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $configFactory = $container->get('config.factory');
    $httpClient = $container->get('http_client');
    $messenger = $container->get('messenger');

    return new static($configFactory, $httpClient, $messenger);
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    Client $http_client,
    MessengerInterface $messanger_manager
  ) {
    $this->config = $config_factory->get('markdown_exporter.settings');
    $this->configEditable = $config_factory;
    $this->httpClient = $http_client;
    $this->messenger = $messanger_manager;
  }

  /**
   * Reload the previous page.
   */
  public function downloadFile() {
    $mdDir = 'public://' . $this->config->get('markdown_exporter_dir');
    $zipFile = $this->config->get('markdown_exporter_dir', 'markdown_export') . '_' . \Drupal::time()->getRequestTime() . '.zip';
    $zipUri = 'temporary://' . $zipFile;

    $zip = new \ZipArchive();
    if ($handle = opendir($mdDir)) {
      $result = $zip->open(\Drupal::service('file_system')->realpath($zipUri), \ZipArchive::CREATE);
      if (!$result) {
        throw new DriverException('Zip archive could not be created. Error ' . $result);
      }

      while (FALSE !== ($fileName = readdir($handle))) {
        if ($fileName != '.' && $fileName != '..') {
          $zip->addFile(\Drupal::service('file_system')->realpath($mdDir) . '/' . $fileName, $fileName);
        }
      }
      closedir($handle);
      $result = $zip->close();
      if (!$result) {
        throw new DriverException('Zip archive could not be closed.');
      }
    }
    $response = new Response();
    $response->headers->set('Content-Type', 'text/html; charset=utf-8');
    $response->header->set("Cache-Control: public");
    $response->header->set("Content-Type: application/zip");
    $response->header->set("Cache-Control: no-store, no-cache, must-revalidate");
    $response->header->set("Cache-Control: post-check=0, pre-check=0");
    $response->header->set("Content-Disposition: attachment; filename=\"$zipFile\";");
    $response->header->set("Content-Transfer-Encoding: binary");

    $filePointer = fopen(\Drupal::service('file_system')->realpath($zipUri), 'rb');
    fpassthru($filePointer);

    return $response;
  }

  /**
   * Authorize with GitHub to generate access token and redirect.
   */
  public function githubReauthorize(Request $request) {
    $queryparams = $request->query->get('code');
    if ($queryparams) {
      /* need to swap this code for a permanent access_token */
      $url = 'https://github.com/login/oauth/access_token';
      $headers = ['Accept' => 'application/json'];
      $params = [
        'client_id' => $this->config->get('github_oauth_app_clientid'),
        'client_secret' => $this->config->get('github_oauth_app_clientsecret'),
        'code' => $queryparams,
        'state' => $this->config->get('github_state_var'),
      ];
      $result = $this->httpClient->post($url, [
        'form_params' => $params,
        // 'body' => http_build_query($params),
        'headers' => $headers,
        'http_errors' => FALSE,
      ]);
      $resp = $result->getBody()->getContents();
      $response = \json_decode($resp, TRUE);
      /* store the authorised token */
      if (array_key_exists('access_token', $response)) {
        $this->configEditable->getEditable('markdown_exporter.settings')
          ->set('github_access_token', $response['access_token'])
          ->save();
        /* add a success message */
        $this->messenger->addMessage($this->t('GitHub authorization complete.'), 'status');
      }
      else {
        /* add a success message */
        $this->messenger->addMessage($this->t('Already authenticated with GitHub.'), 'status');
      }

      return $this->redirect('markdown_exporter.github_auth');
    }
  }

}
