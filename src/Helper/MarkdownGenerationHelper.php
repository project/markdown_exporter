<?php

namespace Drupal\markdown_exporter\Helper;

use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\RequestException;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Markdownify\Converter;
use GuzzleHttp\Client;
use Drupal\markdown_exporter\MarkdownGenerationHelperInterface;

/**
 * Class MarkdownGenerationHelper.
 *
 * @package Drupal\markdown_exporter\Helper
 */
class MarkdownGenerationHelper implements MarkdownGenerationHelperInterface {

  /**
   * Account Interface.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The markdown configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The drupal messenger.
   *
   * @var Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The logger factory.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The stream wrapper manager.
   *
   * @var Drupal\Core\StreamWrapper\StreamWrapperManager
   */
  protected $wrapperManager;

  /**
   * Guzzle Http Client.
   *
   * @var GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * GitHub API URL.
   *
   * @var stringgithubApiEndpoint
   */
  private $githubApiEndpoint = "https://api.github.com/";

  /**
   * Class constructor.
   */
  public function __construct(
        ConfigFactoryInterface $config_factory,
        AccountInterface $account,
        Client $http_client,
        MessengerInterface $messanger_manager,
        LoggerChannelFactory $factory,
        StreamWrapperManagerInterface $wrapper_manager
  ) {
    $this->account = $account;
    $this->config = $config_factory->get('markdown_exporter.settings');
    $this->httpClient = $http_client;
    $this->messenger = $messanger_manager;
    $this->loggerFactory = $factory;
    $this->wrapperManager = $wrapper_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
    $container->get('current_user'),
    $container->get('http_client')
    );
  }

  /**
   * Helper reusuable function Generate markdown content.
   *
   * Converts the content of the node to markdown.
   *
   * @param string $url_alias
   *   Url alias.
   * @param Drupal\Core\Entity\EntityInterface $node
   *   Node by entity interface.
   * @param array $entity_fields
   *   Entity fields.
   */
  public function mdContentGenerate($url_alias, EntityInterface $node, array $entity_fields = []) {
    $mdObj = new Converter(0, MDFY_BODYWIDTH, MDFY_KEEPHTML);
    $bundle = $node->bundle();

    if (array_key_exists($bundle, $entity_fields) && !empty($entity_fields[$bundle])) {

      $formatter_fields = $entity_fields[$bundle]['formatter'];
      array_unshift($formatter_fields, "type", "title", "created");
      $content_fields = $entity_fields[$bundle]['content'];
      if (!array_key_exists('body', $entity_fields[$bundle]['content'])) {
        $content_fields = array_unshift($content_fields, 'body');
      }
    }
    else {
      $formatter_fields = ['type', 'title', 'created'];
      $content_fields = ['body'];
    }

    // Preparing MD formatter.
    $formatter = "---\n";
    $formatter .= "uri: " . $url_alias . "\n";
    foreach ($formatter_fields as $fld1) {
      $field_value = '';
      $formatter_field_data = '';

      switch ($fld1) {
        case 'type':
          $field_value = $fld1 . ": " . $node->bundle() . "\n";
          break;

        case 'title':
          $field_value = $fld1 . ": " . $node->title->value . "\n";
          break;

        case 'created':
          $field_value = $fld1 . ": " . $node->created->value . "\n";
          break;

        case 'author':
          $field_value = $fld1 . ": " . $this->account->getDisplayName() . "\n";
          break;

        default:
          $customFieldVal = $node->get($fld1)->getValue();

          if (isset($customFieldVal)) {
            if (count($customFieldVal) > 1) {
              $field_value .= $fld1 . ":\n";
              foreach ($customFieldVal as $valC) {
                foreach ($valC as $k => $v) {
                  if (!is_array($v)) {
                    $field_value .= "\t" . $k . ":" . $v . "\n";
                  }
                }
              }
            }
            else {
              $formatter_field_data = $node->get($fld1)->getValue();
              if (!is_array($formatter_field_data)) {
                $field_value = $fld1 . ": " . $formatter_field_data . "\n";
              }
              else {
                if (isset($formatter_field_data[$node->language][0]['value'])) {
                  $field_value = $fld1 . ": " . $formatter_field_data[$node->language][0]['value'] . "\n";
                }
                else {
                  if (
                  isset(
                   $formatter_field_data[$node->language][0]['uri']) &&
                    !empty(
                     $formatter_field_data[$node->language][0]['uri']
                    )
                  // For image field.
                  ) {
                    $field_value = $fld1 . ": " . file_create_url($formatter_field_data[$node->language][0]['uri']) . "\n";
                  }
                }
              }
            }
          }
      }
      $formatter .= $field_value;
    }
    $formatter .= "---\n\n";

    // Preparing MD content.
    $mdcontent = '';
    if (is_array($content_fields) && !empty($content_fields)) {
      foreach ($content_fields as $fld2) {
        if (isset($node->{$fld2})) {
          $content_field_data = $node->get($fld2)->getValue();
          $mdcontent .= $mdObj->parseString($content_field_data[0]['value']) . "\r\n";
        }
      }
    }
    // Markdown file content.
    $contents = $formatter . $mdcontent;

    return $contents;
  }

  /**
   * GitHub Response.
   *
   * @param string $url
   *   Url to connect.
   *
   * @return \Drupal\Component\Serialization\Json
   *   Json response of API connect
   */
  public function getJsonResponse($url) {
    $url = $this->githubApiEndpoint . $url;
    $headers = [
      'Accept' => 'application/json',
      'Authorization' => 'token ' . $this->config->get('github_access_token'),
    ];

    $result = $this->httpClient->get($url, [
      'headers' => $headers,
    ]);
    $resp = $result->getBody()->getContents();
    $response = Json::decode($resp);

    return $response;
  }

  /**
   * Push Markdown File to Git Uses GITAPI V3.
   *
   * @param string $fileName
   *   Markdoen file name.
   * @param string $fileObj
   *   File object specifier.
   * @param string $id
   *   Node ID.
   */
  public function pushToGitRepo($fileName = "", $fileObj = "", $id = "") {

    if ($fileName == "") {
      return;
    }

    // Prepare to push to the file to GitHub.
    $path = $this->config->get('github_pushto_repo_dir') == '<root>' ? '' : $this->config->get('github_pushto_repo_dir');
    $token = $this->config->get('github_access_token');
    $repo = $this->config->get('github_pushto_repo');

    // fileName comes with prevailing "/".
    $apiUrl = $this->githubApiEndpoint . 'repos/' . $repo . '/contents/' . $path . $fileName;
    $authKey = 'token ' . $token;
    $fileInfo = NULL;
    $response = NULL;
    $sha = TRUE;
    try {
      $fileInfo = $this->httpClient->get($apiUrl, [
        'headers' => [
          'Content-Type' => 'application/json',
          'Authorization' => $authKey,
        ],
      ]);
    }
    catch (RequestException $e) {
      $resp = $e->getResponse();
      if ($resp->getStatusCode() == 404) {
        $sha = FALSE;
      }
      $resp = $e->getResponse();
      $this->messenger->addMessage($resp->getBody()->getContents(), 'error');
      $this->messenger->addMessage($resp->getStatusCode(), 'error');
      $this->loggerFactory->get('markdown_exporter')->error('Error in committing file with response : %resp.',
      [
        '%resp' => Json::encode($resp->getBody()->getContents()),
      ]);
      $this->loggerFactory->get('markdown_exporter')->error('Full Guzzle response : %resp.',
          [
            '%resp' => Json::encode($resp),
          ]);
    }
    catch (ServerException $e) {
      $resp = $e->getResponse();
      $this->messenger->addMessage($resp->getBody()->getContents(), 'error');
      $this->messenger->addMessage($resp->getStatusCode(), 'error');
      $this->loggerFactory->get('markdown_exporter')->error('Error in committing file with response : %resp.',
      [
        '%resp' => Json::encode($resp->getBody()->getContents()),
      ]);
      $this->loggerFactory->get('markdown_exporter')->error('Full Guzzle response : %resp.',
          [
            '%resp' => Json::encode($resp),
          ]);
    }
    catch (BadResponseException $e) {
      $resp = $e->getResponse();
      $this->messenger->addMessage($resp->getBody()->getContents(), 'error');
      $this->messenger->addMessage($resp->getStatusCode(), 'error');
      $this->loggerFactory->get('markdown_exporter')->error('Error in committing file with response : %resp.',
      [
        '%resp' => Json::encode($resp->getBody()->getContents()),
      ]);
      $this->loggerFactory->get('markdown_exporter')->error('Full Guzzle response : %resp.',
      [
        '%resp' => Json::encode($resp),
      ]);
    }
    catch (Exception $e) {
      $this->messenger->addMessage($e, 'error');
    }

    $data = [];
    if ($sha) {
      $response = $fileInfo->getBody()->getContents();
      $data = Json::decode($response);
    }
    $contents = \file_get_contents($fileObj->getFileUri());
    $content = \base64_encode($contents);
    $stream_wrapper_manager = $this->wrapperManager->getViaUri($fileObj->getFileUri());
    $file_path = $stream_wrapper_manager->realpath();
    $hash = \hash_file('md5', $file_path);

    // Commit Message for file.
    $message = 'Markdown export for node ' . $id . ' committed via GitHub API on ' . date('Y-m-d');
    $committer = [];
    // $user = \Drupal::currentUser();
    $committer['name'] = $this->account->getDisplayName();
    $committer['email'] = $this->account->getEmail();
    $params = [
      'message' => $message,
      'committer' => $committer,
      'content' => $content,
    ];

    // Rely on fileInfo Hash if file not found.
    if (!empty($data)) {
      $params['sha'] = (!$sha && $data['sha'] != $hash) ? $hash : $data['sha'];
    }
    if ($this->config->get('github_pushto_repo_branch')) {
      $params['branch'] = $this->config->get('github_pushto_repo_branch');
    }

    // SHA created with retrieved data / file hash
    // PUT request to place for content api
    // to push the file to GIT.
    $result = NULL;
    try {
      $result = $this->httpClient->put($apiUrl, [
        'json' => $params,
        'headers' => [
          'Content-Type' => 'application/json',
          'Authorization' => $authKey,
        ],
      ]);
    }
    catch (RequestException $e) {
      $resp = $e->getResponse();
      $this->messenger->addMessage($resp->getBody()->getContents(), 'error');
      $this->messenger->addMessage($resp->getStatusCode(), 'error');
      $this->loggerFactory->get('markdown_exporter')->error('Error in committing file with response : %resp.',
         [
           '%resp' => Json::encode($resp->getBody()->getContents()),
         ]);
      $this->loggerFactory->get('markdown_exporter')->error('Full Guzzle response : %resp.',
         [
           '%resp' => Json::encode($resp),
         ]);
    }
    catch (ServerException $e) {
      $resp = $e->getResponse();
      $this->messenger->addMessage($resp->getBody()->getContents(), 'error');
      $this->loggerFactory->get('markdown_exporter')->error('Error in committing file with response : %resp.',
      [
        '%resp' => Json::encode($resp->getBody()->getContents()),
      ]);
      $this->loggerFactory->get('markdown_exporter')->error('Full Guzzle response : %resp.',
      [
        '%resp' => Json::encode($resp),
      ]);
    }
    catch (BadResponseException $e) {
      $resp = $e->getResponse();
      $this->messenger->addMessage($resp->getBody()->getContents(), 'error');
      $this->loggerFactory->get('markdown_exporter')->error('Error in committing file with response : %resp.',
      [
        '%resp' => Json::encode($resp->getBody()->getContents()),
      ]);
      $this->loggerFactory->get('markdown_exporter')->error('Full Guzzle response : %resp.',
      [
        '%resp' => Json::encode($resp),
      ]);
    }
    catch (Exception $e) {
      $this->messenger->addMessage($e, 'error');
    }

    if ($result) {
      $resp = $result->getBody()->getContents();
      /* Drupal watchdog log */
      $this->loggerFactory->get('markdown_exporter')->notice('@type: commited with response : %resp.',
      [
        '@type' => 'markdown_exporter commit',
        '%resp' => $resp,
      ]);
      $this->messenger->addMessage('File commited with response: ' . $resp, 'status');

      return;
    }
    $this->messenger->addMessage("Unexpected error committing file, contact administrator!", 'error');
  }

}
