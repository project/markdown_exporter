<?php

namespace Drupal\markdown_exporter;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface MarkdownGenerationHelper.
 *
 * @package Drupal\markdown_exporter\Helper
 */
interface MarkdownExporterGeneratorInterface {

  /**
   * Creating content files in MD format.
   *
   * Converts the content of the specified types to markdown.
   *
   * Creates a zip containing them.
   *
   * @param array $getTypes
   *   Content types.
   * @param string $mode
   *   Mode=cli if running through drush.
   */
  public function bulkExport(array $getTypes = [], $mode = '');

  /**
   * Creating single node content files in MD format.
   *
   * Converts the content of the node to markdown.
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   Node entity.
   * @param string $mode
   *   Mode=cli for drush.
   */
  public function generateMarkdownNodeContent(EntityInterface $entity, $mode = NULL);

}
