<?php

namespace Drupal\markdown_exporter;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface MarkdownGenerationHelper.
 *
 * @package Drupal\markdown_exporter\Helper
 */
interface MarkdownGenerationHelperInterface {

  /**
   * Helper reusuable function Generate markdown content.
   *
   * Converts the content of the node to markdown.
   *
   * @param string $url_alias
   *   Url alias.
   * @param Drupal\Core\Entity\EntityInterface $node
   *   Node by entity interface.
   * @param array $entity_fields
   *   Entity fields.
   */
  public function mdContentGenerate($url_alias, EntityInterface $node, array $entity_fields = []);

  /**
   * GitHub Response.
   *
   * @param string $url
   *   Url to connect.
   *
   * @return \Drupal\Component\Serialization\Json
   *   Json response of API connect
   */
  public function getJsonResponse($url);

  /**
   * Push Markdown File to Git Uses GITAPI V3.
   *
   * @param string $fileName
   *   Markdoen file name.
   * @param string $fileObj
   *   File object specifier.
   * @param string $id
   *   Node ID.
   */
  public function pushToGitRepo($fileName, $fileObj, $id);

}
