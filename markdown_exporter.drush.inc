<?php
/**
* @file Contains the code to generate the custom drush commands.
*/
/**
* Implements hook_drush_command().
*/
function markdown_exporter_drush_command() {
 $items = array();
 $items['markdown-export'] = [
   'description' => 'Export content by type in md format',
   'arguments' => [
     'ctype' => 'Content type',
   ],
   'drupal dependencies' => ['markdown_exporter'],
   'aliases' => ['mde'],
   'examples' => [
    'drush mde' => t('Generates markdown files for node across all content types')
   ]
 ];
 return $items;
}
/**
* Call back function drush_custom_drush_command_say_hello()
* The call back function name in the  following format
*   drush_{module_name}_{item_id_for_command}()
*/
function drush_markdown_exporter_markdown_export($name = '') {
  // to include the inc file with content_export_in_markdown() function
  module_load_include('inc', 'markdown_exporter', 'markdown_exporter');

  // preparing variables: args, content types
  $args = func_get_args(); // argument supplied with drush command
  $content_types = array_keys(node_type_get_names());
  $exported_types = [];

  if (!empty($args)) {
    foreach ($args as $ntype) {
        if (in_array($ntype, $content_types)) {
            $exported_types[] = $ntype;
        }
    }
    if (empty($exported_types)) { // if none of the content types are valid
        \Drupal::messenger()->addStatus(t('Invalid argument supplied with the Drush command!'));
        return;
    }
  }
  $exported_types = $content_types;

  \Drupal::service('markdown_exporter.marekdown_generator')->bulkExport($exported_types, 'cli');
}